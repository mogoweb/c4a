#!/bin/bash
CURRENT_DIR="$(readlink -f "$(dirname $BASH_SOURCE)/../../")"
if [[ -z "${CHROME_SRC}" ]]; then
  # If $CHROME_SRC was not set, assume current directory is CHROME_SRC.
  export CHROME_SRC="${CURRENT_DIR}"
fi

if [[ "${CURRENT_DIR/"${CHROME_SRC}"/}" == "${CURRENT_DIR}" ]]; then
  # If current directory is not in $CHROME_SRC, it might be set for other
  # source tree. If $CHROME_SRC was set correctly and we are in the correct
  # directory, "${CURRENT_DIR/"${CHROME_SRC}"/}" will be "".
  # Otherwise, it will equal to "${CURRENT_DIR}"
  echo "Warning: Current directory is out of CHROME_SRC, it may not be \
the one you want."
  echo "${CHROME_SRC}"
fi

cd ${CHROME_SRC}/third_party/skia
git format-patch 6ffc0ce314009547c289a76e8573a0d758dde280^@
for f in *.patch; do mv $f ${CHROME_SRC}/c4a/patch/patches/skia_$f; done
