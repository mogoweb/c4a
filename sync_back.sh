#!/bin/bash

CURRENT_DIR="$(readlink -f "$(dirname $BASH_SOURCE)/..")"
if [[ -z "${CHROME_SRC}" ]]; then
  # If $CHROME_SRC was not set, assume current directory is CHROME_SRC.
  export CHROME_SRC="${CURRENT_DIR}"
fi

if [[ "${CURRENT_DIR/"${CHROME_SRC}"/}" == "${CURRENT_DIR}" ]]; then
  # If current directory is not in $CHROME_SRC, it might be set for other
  # source tree. If $CHROME_SRC was set correctly and we are in the correct
  # directory, "${CURRENT_DIR/"${CHROME_SRC}"/}" will be "".
  # Otherwise, it will equal to "${CURRENT_DIR}"
  echo "Warning: Current directory is out of CHROME_SRC, it may not be \
the one you want."
  echo "${CHROME_SRC}"
fi

rsync -avz ${CHROME_SRC}/c4a/testshell/java/src/com/mogoweb/browser/ ${CHROME_SRC}/c4a/mogo/java/src/com/mogoweb/browser
# sync res
rsync -avz ${CHROME_SRC}/c4a/testshell/java/res/ ${CHROME_SRC}/c4a/mogo/res
# sync assets
rsync -avz ${CHROME_SRC}/c4a/testshell/java/assets/ ${CHROME_SRC}/c4a/mogo/assets
# sync AndroidManifest.xml
rsync -avz ${CHROME_SRC}/c4a/testshell/java/AndroidManifest.xml ${CHROME_SRC}/c4a/mogo/java/AndroidManifest.xml



