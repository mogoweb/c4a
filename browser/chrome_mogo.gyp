# Copyright (c) 2012 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
{
  'variables': {
    'chromium_code': 1,
    'package_name': 'chromium_mogo',
  },
  'includes': [
    '../../chrome/chrome_android_paks.gypi', # Included for the list of pak resources.
  ],
  'targets': [
    {
      'target_name': 'libchromium_mogo',
      'type': 'shared_library',
      'dependencies': [
        '../../chrome/chrome.gyp:chrome_android_core',
      ],
      'sources': [
        # This file must always be included in the shared_library step to ensure
        # JNI_OnLoad is exported.
        '<(DEPTH)/chrome/app/android/chrome_jni_onload.cc',
        '<(DEPTH)/c4a/browser/chrome_main_delegate_mogo_android.cc',
        '<(DEPTH)/c4a/browser/chrome_main_delegate_mogo_android.h',
        '<(DEPTH)/c4a/browser/mogo_google_location_settings_helper.cc',
        '<(DEPTH)/c4a/browser/mogo_google_location_settings_helper.h',
      ],
      'include_dirs': [
        '<(SHARED_INTERMEDIATE_DIR)/android',
        '<(SHARED_INTERMEDIATE_DIR)/chromium_mogo',
        '../../skia/config',
      ],
      'conditions': [
        [ 'order_profiling!=0', {
          'conditions': [
            [ 'OS=="android"', {
              'dependencies': [ '../../tools/cygprofile/cygprofile.gyp:cygprofile', ],
            }],
          ],
        }],
      ],
    },
    {
      'target_name': 'chromium_mogo',
      'type': 'none',
      'dependencies': [
        '../../media/media.gyp:media_java',
        '../../chrome/chrome.gyp:chrome_java',
        'chromium_mogo_paks',
        'chromium_mogo_assets',
        'libchromium_mogo',
      ],
      'variables': {
        'apk_name': 'ChromiumMogo',
        'manifest_package_name': 'com.mogoweb.browser',
        'java_in_dir': '<(DEPTH)/c4a/browser/java',
        'resource_dir': '../java/res',
        'asset_location': '<(ant_build_out)/../assets/<(package_name)',
        'native_libs_paths': [ '<(SHARED_LIB_DIR)/libchromium_mogo.so', ],
        'additional_input_paths': [
          '<@(chrome_android_pak_output_resources)',
          '<(chrome_android_pak_output_folder)/devtools_resources.pak',
        ],
      },
      'includes': [ '../../build/java_apk.gypi', ],
    },
    {
      # chromium_testshell creates a .jar as a side effect. Any java targets
      # that need that .jar in their classpath should depend on this target,
      # chromium_testshell_java. Dependents of chromium_testshell receive its
      # jar path in the variable 'apk_output_jar_path'.
      'target_name': 'chromium_mogo_java',
      'type': 'none',
      'dependencies': [
        'chromium_mogo',
      ],
      # This all_dependent_settings is used for java targets only. This will add
      # the chromium_testshell jar to the classpath of dependent java targets.
      'all_dependent_settings': {
        'variables': {
          'input_jars_paths': ['>(apk_output_jar_path)'],
        },
      },
      # Add an action with the appropriate output. This allows the generated
      # buildfiles to determine which target the output corresponds to.
      'actions': [
        {
          'action_name': 'fake_generate_jar',
          'inputs': [],
          'outputs': ['>(apk_output_jar_path)'],
          'action': [],
        },
      ],
    },
    {
      'target_name': 'chromium_mogo_paks',
      'type': 'none',
      'dependencies': [
        '<(DEPTH)/chrome/chrome_resources.gyp:packed_resources',
        '<(DEPTH)/chrome/chrome_resources.gyp:packed_extra_resources',
      ],
      'copies': [
        {
          'destination': '<(chrome_android_pak_output_folder)',
          'files': [
            '<@(chrome_android_pak_input_resources)',
            '<(SHARED_INTERMEDIATE_DIR)/webkit/devtools_resources.pak',
          ],
        }
      ],
    },
    {
      'target_name': 'chromium_mogo_assets',
      'type': 'none',
      'dependencies': [
      ],
      'copies': [
        {
          'destination': '<(chrome_android_pak_output_folder)',
          'files': [
            '<(DEPTH)/c4a/browser/java/assets/html',
          ],
        }
      ],
    },
  ],
}
