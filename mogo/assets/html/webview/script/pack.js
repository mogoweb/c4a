﻿/**
 * @author AlexBian
 */
 
var cosAjax = {
	
	api : {
		action : "/inter-web/web/gateway/gatewayAction.do",
		nativeGet : "/inter-web/webview/details.html?guid=",
		appShotsPars : {
			requestActionName : "photoListByGUIDExecutor",
			guid : ""
		},
		appIntrPars : {
			requestActionName : "appInfoDeviceRequestExecutor",
			guid : ""
		},
		appRelatedAuthorPars : {
			requestActionName : "appListForDeveloperExecutor",
			guid : ""
		},
		appRelatedViewPars : {
			requestActionName : "showViewAppListExecutor",
			guid : ""
		},
		appCommentsViewPars : {
			requestActionName : "evaluationListExecutor",
			guid : ""
		},
		appCommentsCommitPars : {
			requestActionName : "addEvaluationExecutor",
			guid : ""
		}
	},

    scrollLen: 0,

    appShots: "",
	
	loadPage : function(pars){
		window.location.href = cosAjax.api.nativeGet + pars;
	},
	
	hudMsg : function(type, message, timeOut){
		$('.hudmsg').remove();
		if (!timeOut) {
			timeOut = 3000;
		}
		var timeId = new Date().getTime();
		if (type != '' && message != '') {
			$('<div class="hudmsg ' + type + '" id="msg_' + timeId + '"><img src="images/msg_' + type + '.png" alt="" />' + message + '</div>').hide().appendTo('body').fadeIn();

			var timer = setTimeout(function() {
				$('.hudmsg#msg_' + timeId + '').fadeOut('slow', function() {
					$(this).remove();
				});
			}, timeOut);
		}
	},
	
	loadAppShots : function(getUrl, pars){
		$.ajax({
			type : "get",
			async : false,
			url : getUrl,
			cache: false,
			data : pars,
			dataType: "json",
			success : function(json) {
                $("#loading").remove();
				cosAjax.scrollLen = json.dataList.length;
                cosAjax.appShots = json;
				$.each(json.dataList, function(i){
					var li = $("<li><img src="+ json.dataList[i].imgUrl +" /></li>");
                    var lispan = $("<li><span><span>"+ i +"</span></span></li>");
                    $("#appShots ul").append(li);
                    $("ul.camera_pag_ul").append(lispan);
				});

                if(appShots.imgIShor()){
                    appShots.initHORshot();
                }else{
                    appShots.initVERshot();
                }

			},
			error : function() {
				cosAjax.hudMsg('error', '应用快照数据加载失败!', 2000);
			}
		});
	},
	
	loadAppIntr : function(getUrl, pars, Eltarget){
		$.ajax({
			type : "get",
			async : false,
			url : getUrl,
			cache: false,
			data : pars,
			dataType: "json",
			success : function(json) {
				Eltarget.html(json.dataList[0].goodsDes);
			},
			error : function() {
				cosAjax.hudMsg('error', '应用介绍数据加载失败!', 2000);
			}
		});
	},
	
	loadAppRelated : function(getUrl, pars, Eltarget){
		$.ajax({
			type : "get",
			async : false,
			url : getUrl,
			cache: false,
			data : pars,
			dataType: "json",
			success : function(json) {
				$.each(json.dataList, function(i){
					if(i < 5){
						var aliasName = substr(json.dataList[i].goodsName, 8);
						var li = $("<li><a href=javascript:cosAjax.loadPage('"+ json.dataList[i].guid +"') class=glass><img src="+ json.dataList[i].imgUrl +" /></a><a href=javascript:cosAjax.loadPage('"+ json.dataList[i].guid +"') class=appname>"+ aliasName +"</a></li>");
	                    Eltarget.append(li);
					}	
				});
			},
			error : function() {
				cosAjax.hudMsg('error', '数据加载失败!', 2000);
			}
		});
	}
};

function substr(str, len){
    if(!str || !len){
        return '';
    }
    var a = 0, i = 0, temp = "";
    for (i=0; i<str.length; i++){
        if (str.charCodeAt(i) > 255){
            a+=2;
        }
        else{
            a++;
        }
        if(a > len){
            return temp;
        }

        temp += str.charAt(i);
    }
    return str;
}