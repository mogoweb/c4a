var autoPage = (function(w){

    var loading = $("<li class='comment' style='text-align: center; line-height: 80px; background: #bbb;'>评论加载中...</li>"), loadCount = 1, guid = $.url().param("guid");;

    function bottomReached (intpx){
        var pageHeight = Math.max(document.body.scrollHeight ||
                document.body.offsetHeight);
        var viewportHeight = window.innerHeight  ||
                document.documentElement.clientHeight  ||
                document.body.clientHeight || 0;
        var scrollHeight = window.pageYOffset ||
                document.documentElement.scrollTop  ||
                document.body.scrollTop || 0;
        return pageHeight - viewportHeight - scrollHeight < intpx;
    };

    function stopScroll (scrollCallback){
        $(window).unbind("scrollstop", scrollCallback);
    };

    function startScroll (scrollCallback){
        $(window).bind("scrollstop", scrollCallback);
    };

    function scrollCallback (){
        if(bottomReached(10)){
            stopScroll(scrollCallback);
            $("#listview").append(loading);
            window.scrollTo(0,99999);
            var wait = setTimeout(loadData, 2000);
        }
    };

    function loadData (){
        $.ajax({
            type : "get",
            async : false,
            url : "/inter-web/web/gateway/gatewayAction.do",
            cache: false,
            data : {
                "requestActionName": "evaluationListExecutor",
                "guid": guid,
                "pageSize": 10,
                "pageNumber": loadCount
            },
            dataType: "json",
            success : function(json) {
                if(loadCount == json.pageNumber){
                    loading.remove();
                    stopScroll(scrollCallback);
                    return false;
                }
                else{
                    $.each(json.dataList, function(i){
                        var li = $("<li class='comment'><img src='images/thumbnail_bg.png' class='thumb' /><h3 title='评论者'>"+ json.dataList[i].userName +"</h3><p title='评论内容'>"+ json.dataList[i].comment +"</p><p class='ui-li-aside' title='评论日期'><strong>"+ json.dataList[i].createDate +"</strong></p></li>");
                        $("#listview").append(li);
                    });
                    loadCount++;
                    loading.remove();
                    $("#listview").listview('refresh');
                    startScroll(scrollCallback);
                }

            },
            error : function() {
                alert('评论加载失败!');
            }
        });

    };

    function firstLoadData (){
        $.ajax({
            type : "get",
            async : false,
            url : "/inter-web/web/gateway/gatewayAction.do",
            cache: false,
            data : {
                "requestActionName": "evaluationListExecutor",
                "guid": guid,
                "pageSize": 10,
                "pageNumber": loadCount
            },
            dataType: "json",
            success : function(json) {
                if(json.dataList.length == 0){
                    var li = $("<li>该应用暂时没有评论！</li>");
                    $("#listview").append(li);
                    return false;
                }
                else{
                    $.each(json.dataList, function(i){
                        var li = $("<li class='comment'><img src='images/thumbnail_bg.png' class='thumb' /><h3 title='评论者'>"+ json.dataList[i].userName +"</h3><p title='评论内容'>"+ json.dataList[i].comment +"</p><p class='ui-li-aside' title='评论日期'><strong>"+ json.dataList[i].createDate +"</strong></p></li>");
                        $("#listview").append(li);
                    });
                    loadCount++;
                    $("#listview").listview('refresh');
                    startScroll(scrollCallback);
                }
            },
            error : function() {
                alert('评论加载失败!');
            }
        });

    };

    return {
        doAuto: firstLoadData
    };
})(this);
/**
 * @author AlexBian
 */

$(function(){
	
	function showComments(){

        autoPage.doAuto();

		$("#listview").listview('refresh');
	}
	
	if($.url().attr("file") == "comments.html"){
		showComments()
	}
	if($.url().attr("guid") == ""){}

});