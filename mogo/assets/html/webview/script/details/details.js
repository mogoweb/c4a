/**
 * @author AlexBian
 */
var myScroll, scrollContent;

function shotLoaded() {
    myScroll = new iScroll('appShots', {
        snap: true,
        momentum: false,
        hScrollbar: false,
        onScrollEnd: function () {
            $("#indicator").find("li").removeClass("cameracurrent").eq(this.currPageX).addClass("cameracurrent");
            /*document.querySelector('#indicator > li.cameracurrent').className = '';
            document.querySelector('#indicator > li:nth-child(' + (this.currPageX+1) + ')').className = 'cameracurrent';*/
            window.count = this.currPageX;
        },
        onTouchEnd: function(){
            $("#indicator").find("li").removeClass("cameracurrent").eq(this.currPageX).addClass("cameracurrent");
        }
    });
}
document.addEventListener('DOMContentLoaded', shotLoaded, false);

function contentLoaded() {
    scrollContent = new iScroll('contentWrapper', {
        vScrollbar: false
    });
}
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
document.addEventListener('DOMContentLoaded', contentLoaded, false);

var appShots = {

    imgIShor: function(){
        var imgwidth = $("#thelist li img").width();
        var imgheight = $("#thelist li img").height();
        if(imgwidth > imgheight){
            return true;
        }
        else{
            return false;
        }
    },

    DEVW: 720,

    SHOTW: 512,

    SHOTH: 300,

    SHOTSTEP: 680,

    initHORshot: function(){
        $("#thelist li img").attr({width: appShots.SHOTW, height: appShots.SHOTH});
        $("#appShots").css({height: appShots.SHOTH + 14});
        $("#thelist").css({width: appShots.SHOTSTEP * cosAjax.scrollLen, height: appShots.SHOTH + 14});
        $("#slider").css({width: appShots.SHOTSTEP * cosAjax.scrollLen});
        $("ul.camera_pag_ul li").first().addClass("cameracurrent");
    },

    initVERshot: function(){
        $("#thelist li img").attr({width: appShots.SHOTW, height: appShots.SHOTH});
        $("#appShots").css({height: appShots.SHOTH + 14});
        $("#thelist").css({width: appShots.SHOTSTEP * cosAjax.scrollLen, height: appShots.SHOTH + 14});
        $("#slider").css({width: appShots.SHOTSTEP * cosAjax.scrollLen});
        $("ul.camera_pag_ul li").first().addClass("cameracurrent");

        document.getElementById("thelist").addEventListener("click", function(ev){
            appShots.showBig();
        }, false);

        $("#thelistBig").live("click", appShots.hideBig);
    },

    initBig: function(){
        var data = cosAjax.appShots;
        var temp = "", li = "";
        $.each(data.dataList, function(i){
            temp = "<li><img src="+ data.dataList[i].imgUrl +" /></li>";
            li += temp;
        });

        $("#thelistBig").html(li);

        var widthDev = 720, heightWin = 720, heightView = $(document).height();

        $("#appShotsBig").css({top: (heightView-720)/2});
        $("#thelistBig").css({width: widthDev * cosAjax.scrollLen, height: heightWin + 14});
        $("#sliderBig").css({width: widthDev * cosAjax.scrollLen});

        var myBigScroll;
        function shotBigLoaded() {
            myBigScroll = new iScroll('appShotsBig', {
                snap: true,
                momentum: false,
                hScrollbar: false,
                bounceLock: true,
                x: -(window.count * 720)
            });
        }
        shotBigLoaded();
    },

    showBig: function(){
        if($("#appShotsBig").length > 0 && $("#sliderBig").length > 0){
            if(window.count == undefined){
                window.count = 0;
            }
            $("div.overlay").show();
            $("#appShotsBig").show();
            appShots.initBig();
            return false;
        }
        if($("#appShotsBig").length == 0){
            $("#noWrap").html("<div id=appShotsBig class=appshotsBig><div id=sliderBig><ul id=thelistBig></ul></div></div>");
            $("div.overlay").show();
            $("#appShotsBig").show();
            appShots.initBig();
            return false;
        }
        else{
            $("div.overlay").show();
            $("#appShotsBig").show();
            appShots.initBig();
        }

    },

    hideBig: function(){
        $("div.overlay").hide();
        $("#appShotsBig").hide();
        $("#noWrap").html("");
    }
};

$(function(){

	function showDetails(){
		cosAjax.api.appShotsPars.guid = $.url().param("guid");
		cosAjax.api.appIntrPars.guid = $.url().param("guid");
		cosAjax.api.appRelatedAuthorPars.guid = $.url().param("guid");
		cosAjax.api.appRelatedViewPars.guid = $.url().param("guid");
		
		cosAjax.loadAppShots(cosAjax.api.action, cosAjax.api.appShotsPars);
		cosAjax.loadAppIntr(cosAjax.api.action, cosAjax.api.appIntrPars, $("#appIntr"));
		cosAjax.loadAppRelated(cosAjax.api.action, cosAjax.api.appRelatedAuthorPars, $("#relatedApp1"));
		cosAjax.loadAppRelated(cosAjax.api.action, cosAjax.api.appRelatedViewPars, $("#relatedApp2"));
		
		$("div.articleTitle").first().css({marginTop: 5});
        var footerList = ((appShots.DEVW - 40) - (96 * 5)) / 10 ;
		$("ul.iconList li").css({marginLeft : footerList, marginRight : footerList});
		
	}
	
	if($.url().attr("file") == "details.html"){
		showDetails();
	}
	if($.url().attr("guid") == ""){}
	
});