/**
 * @class Browser
 * @constructor
 * @description 判断浏览器类型，兼容ie，ff，chrome，opera，webkit
 */
function Browser () {
    var browserName = window.navigator.userAgent.toLowerCase();
    this.ie = /msie/.test(browserName);
    this.ff = /gecko/.test(browserName);
    this.chrome = /chrome/.test(browserName);
    this.opera = /opera/.test(browserName);
    this.webkit = /webkit/.test(browserName);
};

/**
 * @class ImgReady
 * @constructor
 * @description 判断图片是否加载完成
 */
function ImgReady (url, callbackSuc, callbackErr) {
    var val = url,
            img = new Image(),
            browser = new Browser();

    if (browser.ie) {
        img.onreadystatechange = function () {
            //gif等动态图片，这些动态图片可能会多次触发onload
            img.onreadystatechange = null;
            if (img.readyState == "complete" || img.readyState == "loaded") {
                callbackSuc(img);
            }
        }
    }
    else if (browser.ff || browser.chrome || browser.webkit) {
        img.onload = function () {
            img.onload = null;
            if (img.complete == true) {
                callbackSuc(img);
            }
        }
    }

    img.onerror = function () {
        callbackErr();
    };

    img.src = val;
};

/**
 * 提示信息方法
 * @name hudMsg
 * @function
 * @private
 * @param {type, message, timeOut} 参数：提示类型， 提示内容， 显示时间长度
 * @description 根据类型显示提示内容，可考虑封装到通用类
 */
function hudMsg (type, message, timeOut) {
    $('.hudmsg').remove();
    if (!timeOut) {
        timeOut = 3000;
    }
    var timeId = new Date().getTime();
    if (type != '' && message != '') {
        $('<div class="hudmsg ' + type + '" id="msg_' + timeId + '"><img src="images/msg_' + type + '.png" alt="" />' + message + '</div>').hide().appendTo('body').fadeIn();

        var timer = setTimeout(function () {
            $('.hudmsg#msg_' + timeId + '').fadeOut('slow', function () {
                $(this).remove();
            });
        }, timeOut);
    }
};

/**
 * 截取应用名称单字节和双字节（混和）字符串方法
 * @name substr
 * @function
 * @private
 * @param {str, len} 参数：字符串， 截取长度
 * @description 截取应用名称单字节和双字节（混和）字符串， 可考虑封装到通用类
 */
function substr (str, len) {
    if (!str || !len) {
        return '';
    }
    var a = 0, i = 0, temp = "";
    for (i = 0; i < str.length; i++) {
        if (str.charCodeAt(i) > 255) {
            a += 2;
        }
        else {
            a++;
        }
        if (a > len) {
            return temp;
        }

        temp += str.charAt(i);
    }
    return str;
};

/**
 * @class TimeConsuming
 * @constructor
 * @description 计算加载完成所用时间
 * @example
 * var time = new TimeConsuming();
 * //耗时过程
 * var timeOut = time.stop() / 1000 （单位：秒）
 */
function TimeConsuming () {
    this.startTime = (new Date()).getTime();
};

TimeConsuming.prototype.stop = function () {
    return (new Date()).getTime() - this.startTime;
};