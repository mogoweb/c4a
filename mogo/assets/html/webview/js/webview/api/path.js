/**
 * @author AlexBian
 */

var ajaxApi = {
	path : {
		action : "/inter-web/web/gateway/gatewayAction.do",
		nativeGet : "/inter-web/webview/details.html?guid=",
		appShotsPars : {
			requestActionName : "photoListByGUIDExecutor",
			guid : ""
		},
		appIntrPars : {
			requestActionName : "appInfoDeviceRequestExecutor",
			guid : ""
		},
		appRelatedAuthorPars : {
			requestActionName : "appListForDeveloperExecutor",
			guid : ""
		},
		appRelatedViewPars : {
			requestActionName : "showViewAppListExecutor",
			guid : ""
		},
		appCommentsViewPars : {
			requestActionName : "evaluationListExecutor",
			guid : ""
		},
		appCommentsCommitPars : {
			requestActionName : "addEvaluationExecutor",
			guid : ""
		}
	}
};