/**
 * @author AlexBian
 */

var myScroll, pullUpEl, pullUpOffset, generatedCount = 0;

function pullUpAction () {
    setTimeout(function () {
        var el, li, i;
        el = document.getElementById('thelist');

        for (i=0; i<3; i++) {
            li = document.createElement('li');
            li.innerText = 'Generated row ' + (++generatedCount);
            el.appendChild(li, el.childNodes[0]);
        }

        myScroll.refresh();
    }, 1000);
}

function loaded() {
    pullUpEl = document.getElementById('listPullUp');
    pullUpOffset = pullUpEl.offsetHeight;

    myScroll = new iScroll('listWrapper', {
        useTransition: true,
        onRefresh: function () {
            if (pullUpEl.className.match('loading')) {
                pullUpEl.className = '';
                pullUpEl.querySelector('.pullUpLabel').innerHTML = '向上拉伸加载更多...';
            }
        },
        onScrollMove: function () {
            if (this.y < (this.maxScrollY - 5) && !pullUpEl.className.match('flip') && this.dirY > 0) {
                pullUpEl.className = 'flip';
                pullUpEl.querySelector('.pullUpLabel').innerHTML = '松开手指刷新数据...';
                this.maxScrollY = this.maxScrollY;
            } else if (this.y > (this.maxScrollY + 5) && pullUpEl.className.match('flip')) {
                pullUpEl.className = '';
                pullUpEl.querySelector('.pullUpLabel').innerHTML = '向上拉伸加载更多...';
                this.maxScrollY = pullUpOffset;
            }
        },
        onScrollEnd: function () {
            if (pullUpEl.className.match('flip') && this.dirY > 0) {
                pullUpEl.className = 'loading';
                pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中...';
                pullUpAction();
            }
        }
    });

}

document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
document.addEventListener('DOMContentLoaded', function () { setTimeout(loaded, 200); }, false);

$(function(){
	
	function showComments(){
		cosAjax.api.appCommentsViewPars.guid = $.url().param("guid");
		
		cosAjax.loadComments(cosAjax.api.action, cosAjax.api.appCommentsViewPars, $("#thelist"));
	}
	
	if($.url().attr("file") == "comments.html"){
		showComments()
	}
	if($.url().attr("guid") == ""){}
	
});