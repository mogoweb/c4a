/**
 * JavaScript for details.html
 * @description 应用详情
 */

var appDetails = {
    shotsLength:0,
    shotsLength2:0,
    shotsData  :'',
    layoutWidth:0,
    shotsBig   :null,
    showShotBig:true,
    completeState:0
};

var detailsHTML = (function (w) {

    var details = w.appDetails;

    //获取应用加载参数
    ajaxApi.path.appShotsPars.guid = ajaxApi.path.appIntrPars.guid = ajaxApi.path.appRelatedAuthorPars.guid = ajaxApi.path.appRelatedViewPars.guid = $.url().param("guid");

    /**
     * 加载应用快照远程数据信息，构建DOM，应用样式，执行事件绑定方法
     * @name loadAppShots
     * @function
     * @param {url, pars} 参数：远程Api Url， 请求参数
     * @description 方法完成请求远程数据，应用快照的响应式布局，快照Scroll，绑定点击查看大图事件
     */
    function loadAppShots (url, pars) {
        $.ajax({
            type    :"get",
            async   :true,
            url     :url,
            data    :pars,
            dataType:"json",
            success :function (json) {

                //保存快照数据和长度到实例化参数
                var remainder = json.dataList.length % 3;
                if(remainder != 0){
                    details.shotsLength = (json.dataList.length / 3 | 0) + 1;
                }
                else{
                    details.shotsLength = (json.dataList.length / 3 | 0);
                }

                details.shotsLength2 = json.dataList.length;
                details.shotsData = json;

                //插入快照显示结构，及等待图片
                $.each(json.dataList, function (i) {
                    var li = $("<li><img src=images/loading.png class=loading /></li>");
                    $("#appShots ul").append(li);
                });

                //插入状态指示圆点
                for(var i = 0; i < details.shotsLength; i++){
                    var lispan = $("<li><span><span>" + i + "</span></span></li>");
                    $("ul.camera_pag_ul").append(lispan);
                }

                //执行快照响应式布局
                reShotlayout(window.innerWidth);

                //设置延迟对象，并在异步加载完成后执行后续动作
                $.when(loadAppShotsImg())
                        .done(function () {

                            //执行快照Scroll
                            shotScroll();

                            //初始化Scroll状态指示圆点
                            $("ul.camera_pag_ul li").first().addClass("cameracurrent");

                            appDetails.completeState += 1;

                        })
                        .fail(function () {
                        });

                //监测是否绑定点击显示大图
                if (details.showShotBig) {
                    $("#thelist li img").live("click", function () {
                        details.shotsBig = $(this).data("index");
                        showBig();
                    });

                    $("#thelistBig").live("click", hideBig);
                }

            },
            error   :function () {
                hudMsg('error', '应用快照数据加载失败!', 2000);
            }
        });
    };

    /**
     * 完成images数据加载，并监测远程图片完成状态方法
     * @name loadAppShotsImg
     * @function
     * @param {null} 参数：无
     * @description 监测远程图片加载完成后设置快照响应式布局数据，并替换等待图片
     */
    function loadAppShotsImg () {

        //声明图片加载完成数量计数器
        var loadedShotCount = 0;

        //设定延迟对象
        var dtd = $.Deferred();

        $.each(details.shotsData.dataList, function (i) {

            //缓冲DOM节点
            var loadDOM = $("#thelist li").eq(i);

            //异步加载远程图片
            var loadimg = new ImgReady(
                    details.shotsData.dataList[i].imgUrl,
                    function (img) {
                        loadDOM.empty();
                        loadDOM.append(img);
                        loadDOM.find("img").attr({width:details.layoutWidth, height:details.layoutWidth / (720 / 1280), "data-index":i});

                        //记录加载完成的数量
                        loadedShotCount += 1;

                        if (loadedShotCount == details.shotsLength) {

                            //设定完成状态，并返回延迟对象
                            dtd.resolve();
                            return dtd.promise();
                        }
                    }
            );
        });
    };

    /**
     * 加载应用简介信息方法
     * @name loadAppIntr
     * @function
     * @param {url, pars, eltarget} 参数：远程Api Url， 请求参数， 数据插入目标DOM
     * @description 加载完成后显示文字介绍
     */
    function loadAppIntr (url, pars, eltarget) {
        $.ajax({
            type    :"get",
            async   :true,
            url     :url,
            data    :pars,
            dataType:"json",
            success :function (json) {
                eltarget.html(json.dataList[0].goodsDes);
                //调整间距
                $("div.articleTitle").first().css({marginTop: '0.2em'});

                appDetails.completeState += 1;
            },
            error   :function () {
                hudMsg('error', '应用介绍数据加载失败!', 2000);
            }
        });
    };

    /**
     * 加载当前应用相关应用信息方法
     * @name loadAppRelated
     * @function
     * @param {url, pars, eltarget} 参数：远程Api Url， 请求参数， 数据插入目标DOM
     * @description 加载完成后插入应用ICON及应用名称
     */
    function loadAppRelated (url, pars, eltarget) {
        $.ajax({
            type    :"get",
            async   :true,
            url     :url,
            data    :pars,
            dataType:"json",
            success :function (json) {

                //插入相关应用显示结构和内容，只显示前5条记录
                $.each(json.dataList, function (i) {
                    if (i < 5) {
                        var aliasName = substr(json.dataList[i].goodsName, 8);
                        var li = $("<li><a href=details.html?guid=" + json.dataList[i].guid + " class=glass><img src=" + json.dataList[i].imgUrl + " /></a><br><a href=details.html?guid=" + json.dataList[i].guid + " class=appname>" + aliasName + "</a></li>");
                        eltarget.append(li);
                    }
                });

                appDetails.completeState += 1;

                //执行Icon响应式布局
                reIconlayout(window.innerWidth);
            },
            error   :function () {
                hudMsg('error', '数据加载失败!', 2000);
            }
        });
    };

    /**
     * 应用快照响应式布局方法
     * @name reShotlayout
     * @function
     * @private
     * @param {viewport} 参数：当前布局显示宽度
     * @description 根据当前布局宽度响应式的设置快照相关元素布局数值
     */
    function reShotlayout (viewport) {
        var layout = viewport, resWidth = layout * (190 / 720), resHeight = resWidth / (720 / 1280), layoutPadding = layout * (9 / 720), itemMargin = layout * (22 / 720);

        details.layoutWidth = resWidth;
        $("div.shotWarp").css({paddingLeft: layoutPadding, paddingRight: layoutPadding});
        $("#slider").css({width:(layout - layoutPadding * 2) * details.shotsLength, height: resHeight});
        $("#thelist").css({width:(layout - layoutPadding * 2) * details.shotsLength, height: resHeight});
        $("#thelist li").css({width:resWidth, height:resHeight, lineHeight:resHeight, marginLeft:itemMargin, marginRight:itemMargin});
        $("#thelist li img.loading").css({marginTop:(resHeight - 64) / 2});
    };

    /**
     * 相关应用响应式布局方法
     * @name resIconlayout
     * @function
     * @private
     * @param {viewport} 参数：当前布局显示宽度
     * @description 根据当前布局宽度响应式的设置快照相关元素布局数值
     */
    function reIconlayout (viewport) {
        var layout = viewport, resWidth = layout / 5, resHeight = resWidth * (720 / 1280);

        $(".iconList li").css({width:resWidth});
    };

    /**
     * 应用快照Scroll配置方法
     * @name shotScroll
     * @function
     * @private
     * @param {null} 参数：无
     * @description 实例化iScroll对象，使快照产生carousal效果
     */
    function shotScroll () {
        var myScroll = new iScroll('appShots', {
            snap       :true,
            momentum   :true,
            hScrollbar :false,
            useTransition:true,
            onScrollEnd:function () {
                $("#indicator").find("li").removeClass("cameracurrent").eq(this.currPageX).addClass("cameracurrent");
            },
            onTouchEnd :function () {
                $("#indicator").find("li").removeClass("cameracurrent").eq(this.currPageX).addClass("cameracurrent");
                $("#buginfo span").eq(0).html(window.innerWidth).css('color','red');
                $("#buginfo span").eq(1).html($("#contentWrapper").width()).css('color','red');;
                $("#buginfo span").eq(2).html($(".shotWarp").width()).css('color','red');;
                $("#buginfo span").eq(3).html($("#appShots").width()).css('color','red');;
                $("#buginfo span").eq(4).html($("#slider").width()).css('color','red');;
            }
        });
    };

    /**
     * page Scroll配置方法
     * @name contentLoaded
     * @function
     * @private
     * @param {null} 参数：无
     * @description 实例化iScroll对象，使page产生carousal效果
     */
    function contentLoaded () {
        scrollContent = new iScroll('contentWrapper', {
            vScrollbar:false
        });

        document.addEventListener('touchmove', function (e) {
            e.preventDefault();
        }, false);
        document.addEventListener('DOMContentLoaded', contentLoaded, false);
    };

    /**
     * 创建大图方法
     * @name createBig
     * @function
     * @private
     * @param {null} 参数：无
     * @description 创建大图显示结构和重新布局显示
     */
    function createBig () {
        var data = details.shotsData, temp = "", li = "";

        //生成大图结构和内容
        $.each(data.dataList, function (i) {
            temp = "<li><img src=" + data.dataList[i].imgUrl + " /></li>";
            li += temp;
        });

        //插入大图结构和内容
        $("#thelistBig").html(li);

        //重新布局
        $("#thelistBig").css({width:window.innerWidth * details.shotsLength2});
        $("#sliderBig").css({width:window.innerWidth * details.shotsLength2});
        $("#sliderBig li img").css({width:window.innerWidth, height:window.innerHeight});

        //实例化大图Scroll
        var shotBigLoaded = function() {
            var myBigScroll = new iScroll('appShotsBig', {
                snap      :true,
                momentum  :false,
                hScrollbar:false,
                bounceLock:true,
                x         :-(details.shotsBig * window.innerWidth)
            });
        };
        shotBigLoaded();
    };

    /**
     * 显示大图方法
     * @name showBig
     * @function
     * @private
     * @param {null} 参数：无
     * @description 根据大图显示情况，以不同方式实现大图显示
     */
    function showBig () {

        //第一次显示大图
        if ($("#appShotsBig").length > 0 && $("#sliderBig").length > 0) {
            if (details.shotsBig == null) {
                details.shotsBig = 0;
            }
            $("div.overlay").show();
            $("#appShotsBig").show();
            createBig();
            return false;
        }

        if ($("#appShotsBig").length == 0) {
            $("#noWrap").html("<div id=appShotsBig class=appshotsBig><div id=sliderBig><ul id=thelistBig></ul></div></div>");
            $("div.overlay").show();
            $("#appShotsBig").show();
            createBig();
            return false;
        }
        else {
            $("div.overlay").show();
            $("#appShotsBig").show();
            createBig();
        }
    };

    /**
     * 关闭大图方法
     * @name hideBig
     * @function
     * @private
     * @param {null} 参数：无
     * @description 关闭大图显示
     */
    function hideBig () {
        $("div.overlay").hide();
        $("#appShotsBig").hide();
        $("#noWrap").html("");
    };

    //暴露接口
    return {
        loadS:loadAppShots,
        loadI:loadAppIntr,
        loadR:loadAppRelated,
        cL:contentLoaded
    };

})(this);

//运行
$(function(){

    detailsHTML.loadS(ajaxApi.path.action, ajaxApi.path.appShotsPars);
    detailsHTML.loadI(ajaxApi.path.action, ajaxApi.path.appIntrPars, $("#appIntr"));
    detailsHTML.loadR(ajaxApi.path.action, ajaxApi.path.appRelatedAuthorPars, $("#relatedApp1"));
    detailsHTML.loadR(ajaxApi.path.action, ajaxApi.path.appRelatedViewPars, $("#relatedApp2"));

    var timer = setInterval(function(){
        if(appDetails.completeState == 4){
            var vheight = $("#contentScroller").height();
            $("#contentScroller").height( vheight+20 );
            //执行pageScroll
            detailsHTML.cL();
            clearInterval(timer);
        }
    },10);

    $("#buginfo span").eq(0).html(window.innerWidth).css('color','black');
    $("#buginfo span").eq(1).html($("#contentWrapper").width()).css('color','black');;
    $("#buginfo span").eq(2).html($(".shotWarp").width()).css('color','black');;
    $("#buginfo span").eq(3).html($("#appShots").width()).css('color','black');;
    $("#buginfo span").eq(4).html($("#slider").width()).css('color','black');;

});