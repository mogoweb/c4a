// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "c4a/mogo/chrome_main_delegate_mogo_android.h"

#include "base/android/jni_android.h"
#include "chrome/browser/search_engines/template_url_prepopulate_data.h"

static const char kDefaultCountryCode[] = "US";

ChromeMainDelegateAndroid* ChromeMainDelegateAndroid::Create() {
  return new ChromeMainDelegateMogoAndroid();
}

ChromeMainDelegateMogoAndroid::ChromeMainDelegateMogoAndroid() {
}

ChromeMainDelegateMogoAndroid::~ChromeMainDelegateMogoAndroid() {
}

bool ChromeMainDelegateMogoAndroid::BasicStartupComplete(int* exit_code) {
  TemplateURLPrepopulateData::InitCountryCode(kDefaultCountryCode);
  return ChromeMainDelegateAndroid::BasicStartupComplete(exit_code);
}

bool ChromeMainDelegateMogoAndroid::RegisterApplicationNativeMethods(
    JNIEnv* env) {
  return ChromeMainDelegateAndroid::RegisterApplicationNativeMethods(env);
}
