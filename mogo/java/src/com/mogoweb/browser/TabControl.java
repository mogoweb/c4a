// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package com.mogoweb.browser;

import java.util.ArrayList;
import java.util.List;

import org.chromium.base.JNINamespace;
import org.chromium.chrome.browser.ChromeWebContentsDelegateAndroid;
import org.chromium.chrome.browser.TabBase;
import org.chromium.ui.gfx.NativeWindow;

import android.os.Bundle;
import android.view.Surface;
import android.view.SurfaceHolder;

import com.mogoweb.browser.glue.WebView;

@JNINamespace("chrome")
public class TabControl {

	// Log Tag
    private static final String LOGTAG = "TabControl";

    // next Tab ID, starting at 1
    private static long sNextId = 1;

    private static final String POSITIONS = "positions";
    private static final String CURRENT = "current";

    public static interface OnThumbnailUpdatedListener {
        void onThumbnailUpdated(Tab t);
    }

    // Maximum number of tabs.
    private int mMaxTabs;
    // Private array of WebViews that are used as tabs.
    private ArrayList<Tab> mTabs;
    // Queue of most recently viewed tabs.
    private ArrayList<Tab> mTabQueue;
    // Current position in mTabs.
    private int mCurrentTabPos = -1;
    // the main browser controller
    private final Controller mController;

    private OnThumbnailUpdatedListener mOnThumbnailUpdatedListener;

    // ============== copied from TabManager.java ===========================
    private static final String DEFAULT_URL = "http://www.google.com";

    private NativeWindow mWindow;

    private Tab mCurrentTab;

    /**
     * @param window The window used to generate all ContentViews.
     */
    public void setWindow(NativeWindow window) {
        mWindow = window;
    }

//    /**
//     * @return The currently visible {@link TabBase}.
//     */
//    public TabBase getCurrentTab() {
//        return mCurrentTab;
//    }

    /**
     * Creates a {@link TabBase} with a URL specified by {@code url}.
     * @param url The URL the new {@link TabBase} should start with.
     */
    public void createTab(String url) {

//
//        TabBase tab = new TabBase(getContext(), url, mWindow,
//                new TabManagerWebContentsDelegateAndroid());
//        setCurrentTab(tab);
    }

    /**
     * Put the current tab in the background and set newTab as the current tab.
     * @param newTab The new tab. If newTab is null, the current tab is not
     *               set.
     */
    boolean setCurrentTab(Tab newTab) {
        return setCurrentTab(newTab, false);
    }

    private boolean setCurrentTab(Tab newTab, boolean force) {
        Tab current = getCurrentTab();
        if (current == newTab && !force) {
            return true;
        }
        if (current != null) {
            current.putInBackground();
            mCurrentTabPos = -1;
        }
        if (newTab == null) {
            return false;
        }

        // Move the newTab to the end of the queue
        int index = mTabQueue.indexOf(newTab);
        if (index != -1) {
            mTabQueue.remove(index);
        }
        mTabQueue.add(newTab);

        // Display the new current tab
        mCurrentTabPos = mTabs.indexOf(newTab);
//        WebView mainView = newTab.getWebView();
//        boolean needRestore = !newTab.isSnapshot() && (mainView == null);
//        if (needRestore) {
//            // Same work as in createNewTab() except don't do new Tab()
//            mainView = createNewWebView();
//            newTab.setWebView(mainView);
//        }
        newTab.putInForeground();

        mCurrentTab = newTab;

        mController.getUi().getRenderView().setCurrentContentView(mCurrentTab.getContentView());
        mCurrentTab.getContentView().requestFocus();

        return true;
    }

    // ============== end copy ===========================

    /**
     * Construct a new TabControl object
     */
    public TabControl(Controller controller) {
        mController = controller;
        mMaxTabs = 10/*mController.getMaxTabs()*/; // TODO(alex)
        mTabs = new ArrayList<Tab>(mMaxTabs);
        mTabQueue = new ArrayList<Tab>(mMaxTabs);
    }

    synchronized static long getNextId() {
        return sNextId++;
    }

    /**
     * Return the current tab's main WebView. This will always return the main
     * WebView for a given tab and not a subwindow.
     * @return The current tab's WebView.
     */
    WebView getCurrentWebView() {
        Tab t = getCurrentTab();
        if (t == null) {
            return null;
        }
        return t.getWebView();
    }

    /**
     * Return the current tab's top-level WebView. This can return a subwindow
     * if one exists.
     * @return The top-level WebView of the current tab.
     */
    WebView getCurrentTopWebView() {
        Tab t = getCurrentTab();
        if (t == null) {
            return null;
        }
        return t.getTopWindow();
    }

//    /**
//     * Return the current tab's subwindow if it exists.
//     * @return The subwindow of the current tab or null if it doesn't exist.
//     */
//    WebView getCurrentSubWindow() {
//        Tab t = getTab(mCurrentTab);
//        if (t == null) {
//            return null;
//        }
//        return t.getSubWebView();
//    }

    /**
     * return the list of tabs
     */
    List<Tab> getTabs() {
        return mTabs;
    }

    /**
     * Return the tab at the specified position.
     * @return The Tab for the specified position or null if the tab does not
     *         exist.
     */
    Tab getTab(int position) {
        if (position >= 0 && position < mTabs.size()) {
            return mTabs.get(position);
        }
        return null;
    }

    /**
     * Return the current tab.
     * @return The current tab.
     */
    Tab getCurrentTab() {
        return mCurrentTab;
    }

    /**
     * Return the current tab position.
     * @return The current tab position
     */
    int getCurrentPosition() {
        return mCurrentTabPos;
    }

    /**
     * Given a Tab, find it's position
     * @param Tab to find
     * @return position of Tab or -1 if not found
     */
    int getTabPosition(Tab tab) {
        if (tab == null) {
            return -1;
        }
        return mTabs.indexOf(tab);
    }

    boolean canCreateNewTab() {
        return mMaxTabs > mTabs.size();
    }

    /**
     * Create a new tab.
     * @return The newly createTab or null if we have reached the maximum
     *         number of open tabs.
     */
    Tab createNewTab(boolean privateBrowsing) {
        return createNewTab(null, privateBrowsing);
    }

    Tab createNewTab(Bundle state, boolean privateBrowsing) {
        if (!mController.isContentViewRenderViewInitialized()) return null;
        int size = mTabs.size();
        // Return false if we have maxed out on tabs
        if (!canCreateNewTab()) {
            return null;
        }

        // Create a new tab and add it to the tab list
        Tab t = new Tab(mWindow, mController, state);
        setCurrentTab(t);
        mTabs.add(t);
        // Initially put the tab in the background.
//        t.putInBackground();
        return t;
    }

    /**
     * Create a new tab with default values for closeOnExit(false),
     * appId(null), url(null), and privateBrowsing(false).
     */
    Tab createNewTab() {
        return createNewTab(false);
    }

//    SnapshotTab createSnapshotTab(long snapshotId) {
//        SnapshotTab t = new SnapshotTab(mController, snapshotId);
//        mTabs.add(t);
//        return t;
//    }
//
//    /**
//     * Remove the parent child relationships from all tabs.
//     */
//    void removeParentChildRelationShips() {
//        for (Tab tab : mTabs) {
//            tab.removeFromTree();
//        }
//    }

    /**
     * Remove the tab from the list. If the tab is the current tab shown, the
     * last created tab will be shown.
     * @param t The tab to be removed.
     */
    boolean removeTab(Tab t) {
        if (t == null) {
            return false;
        }

        // Grab the current tab before modifying the list.
        Tab current = getCurrentTab();

        // Remove t from our list of tabs.
        mTabs.remove(t);

        // Put the tab in the background only if it is the current one.
        if (current == t) {
            t.putInBackground();
            mCurrentTabPos = -1;
        } else {
            // If a tab that is earlier in the list gets removed, the current
            // index no longer points to the correct tab.
            mCurrentTabPos = getTabPosition(current);
        }

        // destroy the tab
        t.destroy();
        // clear it's references to parent and children
        t.removeFromTree();

        // Remove it from the queue of viewed tabs.
        mTabQueue.remove(t);
        return true;
    }

    /**
     * Returns the number of tabs created.
     * @return The number of tabs created.
     */
    int getTabCount() {
        return mTabs.size();
    }

    /**
     * Check if the state can be restored.  If the state can be restored, the
     * current tab id is returned.  This can be passed to restoreState below
     * in order to restore the correct tab.  Otherwise, -1 is returned and the
     * state cannot be restored.
     */
    long canRestoreState(Bundle inState, boolean restoreIncognitoTabs) {
        final long[] ids = (inState == null) ? null : inState.getLongArray(POSITIONS);
        if (ids == null) {
            return -1;
        }
        final long oldcurrent = inState.getLong(CURRENT);
        long current = -1;
        if (restoreIncognitoTabs || (hasState(oldcurrent, inState) && !isIncognito(oldcurrent, inState))) {
            current = oldcurrent;
        } else {
            // pick first non incognito tab
            for (long id : ids) {
                if (hasState(id, inState) && !isIncognito(id, inState)) {
                    current = id;
                    break;
                }
            }
        }
        return current;
    }

    private boolean hasState(long id, Bundle state) {
        if (id == -1) return false;
        Bundle tab = state.getBundle(Long.toString(id));
        return ((tab != null) && !tab.isEmpty());
    }

    private boolean isIncognito(long id, Bundle state) {
        Bundle tabstate = state.getBundle(Long.toString(id));
        if ((tabstate != null) && !tabstate.isEmpty()) {
            return tabstate.getBoolean(Tab.INCOGNITO);
        }
        return false;
    }


    /**
     * Creates a new WebView and registers it with the global settings.
     */
//    private WebView createNewWebView() {
//        return createNewWebView(false);
//    }

    /**
     * Creates a new WebView and registers it with the global settings.
     * @param privateBrowsing When true, enables private browsing in the new
     *        WebView.
     */
//    private WebView createNewWebView(boolean privateBrowsing) {
//        return mController.getWebViewFactory().createWebView(privateBrowsing);
//    }

    public void setOnThumbnailUpdatedListener(OnThumbnailUpdatedListener listener) {
        mOnThumbnailUpdatedListener = listener;
        for (Tab t : mTabs) {
            WebView web = t.getWebView();
            if (web != null) {
//                web.setPictureListener(listener != null ? t : null);
            }
        }
    }

    public OnThumbnailUpdatedListener getOnThumbnailUpdatedListener() {
        return mOnThumbnailUpdatedListener;
    }
}
