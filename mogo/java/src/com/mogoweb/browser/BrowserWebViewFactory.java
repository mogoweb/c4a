// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package com.mogoweb.browser;

import org.chromium.content.browser.ContentView;
import org.chromium.content.browser.ContentViewFactory;
import org.chromium.ui.gfx.NativeWindow;

import android.content.Context;

public class BrowserWebViewFactory extends ContentViewFactory {
    public BrowserWebViewFactory() {
        
    }
    
    public ContentView createContentView(Context context, int nativeWebContents,
            NativeWindow nativeWindow, int personality) {
        return new BrowserWebView(context, nativeWebContents, nativeWindow, personality);
    }
}
