// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package com.mogoweb.browser;

import org.chromium.base.PathUtils;
import org.chromium.content.app.LibraryLoader;
import org.chromium.content.browser.ContentSettingsFactory;
import org.chromium.content.browser.ContentViewFactory;
import org.chromium.content.browser.ResourceExtractor;

import android.app.Application;
import android.util.Log;

import com.mogoweb.browser.glue.WebSettingsFactory;

/**
 * A basic test shell {@link Application}.  Handles setting up the native library and
 * loading the right resources.
 */
public class Browser extends Application {
    private final static String LOGTAG = "browser";
    
    // Set to true to enable verbose logging.
    final static boolean LOGV_ENABLED = false;

    // Set to true to enable extra debug logging.
    final static boolean LOGD_ENABLED = true;
    
    private static final String TAG = Browser.class.getCanonicalName();
    private static final String NATIVE_LIBARY = "chromium_mogo";
    private static final String PRIVATE_DATA_DIRECTORY_SUFFIX = "mogo";
    private static final String[] CHROME_MANDATORY_PAKS = {
        "chrome.pak",
        "en-US.pak",
        "resources.pak",
        "chrome_100_percent.pak",
    };

    @Override
    public void onCreate() {
        super.onCreate();
        
        if (LOGV_ENABLED)
            Log.v(LOGTAG, "Browser.onCreate: this=" + this);
        
        BrowserSettings.initialize(getApplicationContext());

        ResourceExtractor.setMandatoryPaksToExtract(CHROME_MANDATORY_PAKS);
        LibraryLoader.setLibraryToLoad(NATIVE_LIBARY);
        PathUtils.setPrivateDataDirectorySuffix(PRIVATE_DATA_DIRECTORY_SUFFIX);
        ContentViewFactory.setInstance(new BrowserWebViewFactory());
        ContentSettingsFactory.setInstance(new WebSettingsFactory());
    }
}