// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package com.mogoweb.browser;

import android.content.Intent;
import android.os.Bundle;

public class CrashRecoveryHandler {
	private static final boolean LOGV_ENABLED = Browser.LOGV_ENABLED;
    private static final String LOGTAG = "BrowserCrashRecovery";

    private static CrashRecoveryHandler sInstance;
    
    private Controller mController;
    private Bundle mRecoveryState = null;
    
    public static CrashRecoveryHandler initialize(Controller controller) {
        if (sInstance == null) {
            sInstance = new CrashRecoveryHandler(controller);
        } else {
            sInstance.mController = controller;
        }
        return sInstance;
    }
    
    public static CrashRecoveryHandler getInstance() {
        return sInstance;
    }

    private CrashRecoveryHandler(Controller controller) {
        mController = controller;
    }
    
    public void startRecovery(Intent intent) {
    	mController.doStart(mRecoveryState, intent);
        mRecoveryState = null;
    }
}
