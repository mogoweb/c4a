// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package com.mogoweb.browser;

import java.util.List;

import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

import com.mogoweb.browser.glue.WebView;

/**
 * UI aspect of the controller
 */
public interface UiController {

    UI getUi();

    WebView getCurrentWebView();

    WebView getCurrentTopWebView();

    Tab getCurrentTab();

    TabControl getTabControl();

    List<Tab> getTabs();

	Tab openTabToHomePage();

    Tab openIncognitoTab();

    Tab openTab(String url, boolean incognito, boolean setActive,
            boolean useCurrent);

    void setActiveTab(Tab tab);

    boolean switchToTab(Tab tab);

    void closeCurrentTab();

    void closeTab(Tab tab);

//    void closeOtherTabs();

    void stopLoading();

    Intent createBookmarkCurrentPageIntent(boolean canBeAnEdit);

//    void bookmarksOrHistoryPicker(ComboViews startView);

    void bookmarkCurrentPage();

    void editUrl();

    void handleNewIntent(Intent intent);

//    void shareCurrentPage();

    void updateMenuState(Tab tab, Menu menu);

    boolean onOptionsItemSelected(MenuItem item);

//    SnapshotTab createNewSnapshotTab(long snapshotId, boolean setActive);

    void loadUrl(Tab tab, String url);

    void setBlockEvents(boolean block);

//    Activity getActivity();

    void showPageInfo();

    void openPreferences();

//    void findOnPage();

    void toggleUserAgent();

    BrowserSettings getSettings();

    boolean supportsVoice();

    void startVoiceRecognizer();
}
