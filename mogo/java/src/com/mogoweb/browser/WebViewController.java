// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package com.mogoweb.browser;

import org.chromium.chrome.browser.ChromeHttpAuthHandler;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Message;

import com.mogoweb.browser.glue.WebView;

/**
 * WebView aspect of the controller
 */
public interface WebViewController {

    Context getContext();

    Activity getActivity();

    TabControl getTabControl();

//    WebViewFactory getWebViewFactory();

    void onSetWebView(Tab tab, WebView view);

    void createSubWindow(Tab tab);

    void onPageStarted(Tab tab, WebView view, Bitmap favicon);

    void onPageFinished(Tab tab);

    void onProgressChanged(Tab tab);

    void onReceivedTitle(Tab tab, final String title);
//
    void onFavicon(Tab tab, WebView view, Bitmap icon);
//
//    boolean shouldOverrideUrlLoading(Tab tab, WebView view, String url);
//
//    boolean shouldOverrideKeyEvent(KeyEvent event);
//
//    boolean onUnhandledKeyEvent(KeyEvent event);
//
//    void doUpdateVisitedHistory(Tab tab, boolean isReload);
//
//    void getVisitedHistory(final ValueCallback<String[]> callback);
//
    void onReceivedHttpAuthRequest(Tab tab, final ChromeHttpAuthHandler handler,
            final String host, final String realm);

//    void onDownloadStart(Tab tab, String url, String useragent, String contentDisposition,
//            String mimeType, String referer, long contentLength);
//
//    void showCustomView(Tab tab, View view, int requestedOrientation,
//            WebChromeClient.CustomViewCallback callback);
//
//    void hideCustomView();
//
//    Bitmap getDefaultVideoPoster();
//
//    View getVideoLoadingProgressView();
//
//    void showSslCertificateOnError(WebView view, SslErrorHandler handler,
//            SslError error);
//
//    void onUserCanceledSsl(Tab tab);
//
//    boolean shouldShowErrorConsole();
//
    void onUpdatedSecurityState(Tab tab);
//
//    void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture);
//
//    void endActionMode();
//
//    void attachSubWindow(Tab tab);
//
//    void dismissSubWindow(Tab tab);

    Tab openTab(String url, boolean incognito, boolean setActive,
            boolean useCurrent);

    Tab openTab(String url, Tab parent, boolean setActive,
            boolean useCurrent);

    boolean switchToTab(Tab tab);

    void closeTab(Tab tab);

    void setupAutoFill(Message message);

    void bookmarkedStatusHasChanged(Tab tab);

    void showAutoLogin(Tab tab);

    void hideAutoLogin(Tab tab);

    boolean shouldCaptureThumbnails();
}
