// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package com.mogoweb.browser;

import org.chromium.ui.gfx.NativeWindow;

import android.content.Context;
import android.util.AttributeSet;

import com.mogoweb.browser.glue.WebView;

public class BrowserWebView extends WebView {

    public interface OnScrollChangedListener {
        void onScrollChanged(int l, int t, int oldl, int oldt);
    }
    
    private OnScrollChangedListener mOnScrollChangedListener;
    
    public BrowserWebView(Context context, int nativeWebContents, NativeWindow nativeWindow, int personality) {
        super(context, nativeWebContents, nativeWindow, personality);
    }
    
    public BrowserWebView(Context context, int nativeWebContents, NativeWindow nativeWindow,
            AttributeSet attrs, int defStyle, int personality) {
        super(context, nativeWebContents, nativeWindow, attrs, defStyle, personality);
    }
    
    public void setOnScrollChangedListener(OnScrollChangedListener listener) {
        mOnScrollChangedListener = listener;
    }
}
