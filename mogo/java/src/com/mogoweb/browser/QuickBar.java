/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mogoweb.browser;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class QuickBar extends LinearLayout implements OnClickListener {
    private ImageButton mBackButton;
    private ImageButton mForwardButton;
    private View mTabSwitcher;

    private UiController mUiController;
    private TitleBar mTitleBar;

    public QuickBar(Context context, UiController controller, TitleBar titlebar) {
        super(context);
        mUiController = controller;
        mTitleBar = titlebar;
        init(context);
    }

    private void init(Context context) {
        LayoutInflater factory = LayoutInflater.from(context);
        factory.inflate(R.layout.quick_bar, this);
        mBackButton = (ImageButton) findViewById(R.id.back);
        mForwardButton = (ImageButton) findViewById(R.id.forward);
        mTabSwitcher = findViewById(R.id.tab_switcher);
        mTabSwitcher.setOnClickListener(this);

        mBackButton.setOnClickListener(this);
        mForwardButton.setOnClickListener(this);
    }

    void updateNavigationState(Tab tab) {
        if (tab != null) {
            mBackButton.setImageResource(tab.canGoBack()
                    ? R.drawable.ic_back_holo_dark
                    : R.drawable.ic_back_disabled_holo_dark);
            mBackButton.setEnabled(tab.canGoBack());
            mForwardButton.setImageResource(tab.canGoForward()
                    ? R.drawable.ic_forward_holo_dark
                    : R.drawable.ic_forward_disabled_holo_dark);
            mForwardButton.setEnabled(tab.canGoForward());
        }
    }

    @Override
    public void onClick(View v) {
        if ((mBackButton == v) && (mUiController.getCurrentTab() != null)) {
            mUiController.getCurrentTab().goBack();
        } else if ((mForwardButton == v)  && (mUiController.getCurrentTab() != null)) {
            mUiController.getCurrentTab().goForward();
        } else if (v == mTabSwitcher) {
            ((PhoneUi) mTitleBar.getUi()).toggleNavScreen();
        }
    }
}
