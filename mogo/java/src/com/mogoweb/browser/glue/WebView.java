package com.mogoweb.browser.glue;

import java.util.Map;

import org.chromium.content.browser.ContentView;
import org.chromium.content.browser.LoadUrlParams;
import org.chromium.content.browser.NavigationHistory;
import org.chromium.ui.gfx.NativeWindow;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Picture;
import android.net.http.SslCertificate;
import android.util.AttributeSet;

public class WebView extends ContentView {
    public static final String SCHEME_TEL = "tel:";

    public interface PictureListener {
        public void onNewPicture(WebView view, Picture picture);
    }

    public WebView(Context context, int nativeWebContents, NativeWindow nativeWindow, int personality) {
        this(context, nativeWebContents, nativeWindow, null,
                android.R.attr.webViewStyle, personality);
    }

    public WebView(Context context, int nativeWebContents, NativeWindow nativeWindow,
            AttributeSet attrs, int defStyle, int personality) {
        super(context, nativeWebContents, nativeWindow, attrs, defStyle, personality);
    }

    public void loadUrl(String url, Map<String, String> headers) {
        loadUrl(new LoadUrlParams(url));
    }

    public void loadUrl(String url) {
        loadUrl(new LoadUrlParams(url));
    }

    public boolean isPrivateBrowsingEnabled() {
        //~:TODO(alex)
        return false;
    }

    public int getContentWidth() {
        return (int)getContentViewCore().getContentWidthCss();
    }

    public int getContentHeight() {
        return (int)getContentViewCore().getContentHeightCss();
    }

    public int getVisibleTitleHeight() {
        //~:TODO(alex)
        return 0;
    }

    public void drawContent(Canvas canvas) {
        Bitmap bitmap = getBitmap();

        canvas.drawBitmap(bitmap, null, null);
    }

    public void setWebViewClient(WebViewClient webviewClient) {
        setContentViewClient(webviewClient);
    }

    public String getOriginalUrl() {
        NavigationHistory history = getContentViewCore().getNavigationHistory();
        int currentIndex = history.getCurrentEntryIndex();
        if (currentIndex >= 0 && currentIndex < history.getEntryCount()) {
            return history.getEntryAtIndex(currentIndex).getOriginalUrl();
        }
        return null;
    }

    public String getTouchIconUrl() {
        return null;
    }

    public Bitmap getFavicon() {
        NavigationHistory history = getContentViewCore().getNavigationHistory();
        int currentIndex = history.getCurrentEntryIndex();
        if (currentIndex >= 0 && currentIndex < history.getEntryCount()) {
            return history.getEntryAtIndex(currentIndex).getFavicon();
        }
        return null;
    }

    public WebSettings getSettings() {
        return (WebSettings)getContentSettings();
    }

    public SslCertificate getCertificate () {
//        return getContentViewCore().getCertificate();
        return null;
    }

}
