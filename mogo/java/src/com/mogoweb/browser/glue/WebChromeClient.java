package com.mogoweb.browser.glue;

import android.graphics.Bitmap;
import android.os.Message;
import android.view.View;

public class WebChromeClient {

    public boolean onCreateWindow(WebView view, final boolean dialog,
            final boolean userGesture, final Message resultMsg) {
        return true;
    }
    
    public void onRequestFocus(WebView view) {
        
    }
    
    public void onCloseWindow(WebView window) {
        
    }
    
    public void onProgressChanged(WebView view, int newProgress) {
    }
    
    public void onReceivedTitle(WebView view, final String title) {
        
    }
    
    public void onReceivedIcon(WebView view, Bitmap icon) {
        
    }
    
    public void onReceivedTouchIconUrl(WebView view, String url,
            boolean precomposed) {
        
    }
    
//    public void onShowCustomView(View view,
//            WebChromeClient.CustomViewCallback callback) {
//        
//    }
//    
//    public void onShowCustomView(View view, int requestedOrientation,
//            WebChromeClient.CustomViewCallback callback) {
//        
//    }
}
