package com.mogoweb.browser.glue;

import org.chromium.content.browser.ContentSettings;
import org.chromium.content.browser.ContentViewCore;

public class WebSettings extends ContentSettings {
    WebSettings(ContentViewCore contentViewCore, int nativeContentView,
            boolean isAccessFromFileURLsGrantedByDefault) {
        super(contentViewCore, nativeContentView, isAccessFromFileURLsGrantedByDefault);
    }
}
