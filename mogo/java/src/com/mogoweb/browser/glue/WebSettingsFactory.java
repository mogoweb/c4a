package com.mogoweb.browser.glue;

import org.chromium.content.browser.ContentSettings;
import org.chromium.content.browser.ContentSettingsFactory;
import org.chromium.content.browser.ContentViewCore;

public class WebSettingsFactory extends ContentSettingsFactory {

    public WebSettingsFactory() {
        
    }
    
    public ContentSettings createContentSettings(ContentViewCore contentViewCore, int nativeContentView,
            boolean isAccessFromFileURLsGrantedByDefault) {
        return new WebSettings(contentViewCore, nativeContentView, isAccessFromFileURLsGrantedByDefault);
    }
}
