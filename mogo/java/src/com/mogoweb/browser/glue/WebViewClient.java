package com.mogoweb.browser.glue;

import org.chromium.chrome.browser.ChromeHttpAuthHandler;
import org.chromium.content.browser.ContentViewClient;

import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Message;
import android.view.KeyEvent;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceResponse;

public class WebViewClient extends ContentViewClient {

    public static int ERROR_FILE_NOT_FOUND = 0;

    public void onPageStarted(WebView view, String url, Bitmap favicon) {

    }

    public void onPageFinished(WebView view, String url) {

    }

    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return false;
    }

    public void onLoadResource(WebView view, String url) {

    }

    public void onReceivedError(WebView view, int errorCode,
            String description, String failingUrl) {

    }

    public void onFormResubmission(WebView view, final Message dontResend,
            final Message resend) {

    }

    public void doUpdateVisitedHistory(WebView view, String url,
            boolean isReload) {

    }

    public void onReceivedSslError(final WebView view,
            final SslErrorHandler handler, final SslError error) {

    }

    public void onProceededAfterSslError(WebView view, SslError error) {

    }

//    public void onReceivedClientCertRequest(final WebView view,
//            final ClientCertRequestHandler handler, final String host_and_port) {
//
//    }

    public WebResourceResponse shouldInterceptRequest(WebView view,
            String url) {
        return null;
    }

    public boolean shouldOverrideKeyEvent(WebView view, KeyEvent event) {
        return false;
    }

    public void onUnhandledKeyEvent(WebView view, KeyEvent event) {

    }

    public void onReceivedLoginRequest(WebView view, String realm,
            String account, String args) {

    }
}
