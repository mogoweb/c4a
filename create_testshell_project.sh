#!/bin/bash

CURRENT_DIR="$(readlink -f "$(dirname $BASH_SOURCE)/..")"
if [[ -z "${CHROME_SRC}" ]]; then
  # If $CHROME_SRC was not set, assume current directory is CHROME_SRC.
  export CHROME_SRC="${CURRENT_DIR}"
fi

if [[ "${CURRENT_DIR/"${CHROME_SRC}"/}" == "${CURRENT_DIR}" ]]; then
  # If current directory is not in $CHROME_SRC, it might be set for other
  # source tree. If $CHROME_SRC was set correctly and we are in the correct
  # directory, "${CURRENT_DIR/"${CHROME_SRC}"/}" will be "".
  # Otherwise, it will equal to "${CURRENT_DIR}"
  echo "Warning: Current directory is out of CHROME_SRC, it may not be \
the one you want."
  echo "${CHROME_SRC}"
fi

cp ${CHROME_SRC}/chrome/android/testshell/java -r ${CHROME_SRC}/c4a/testshell
find ${CHROME_SRC}/c4a/testshell -name ".svn" |xargs rm -rf
cp ${CHROME_SRC}/base/android/java/src -r ${CHROME_SRC}/c4a/testshell/java/
find ${CHROME_SRC}/c4a/testshell -name ".svn" |xargs rm -rf
cp ${CHROME_SRC}/chrome/android/java/src -r ${CHROME_SRC}/c4a/testshell/java/
find ${CHROME_SRC}/c4a/testshell -name ".svn" |xargs rm -rf
cp ${CHROME_SRC}/chrome/browser/component/navigation_interception/java/src -r ${CHROME_SRC}/c4a/testshell/java/
find ${CHROME_SRC}/c4a/testshell -name ".svn" |xargs rm -rf
cp ${CHROME_SRC}/chrome/browser/component/web_contents_delegate_android/java/src -r ${CHROME_SRC}/c4a/testshell/java/
find ${CHROME_SRC}/c4a/testshell -name ".svn" |xargs rm -rf
cp ${CHROME_SRC}/content/public/android/java/src -r ${CHROME_SRC}/c4a/testshell/java/
find ${CHROME_SRC}/c4a/testshell -name ".svn" |xargs rm -rf
rm ${CHROME_SRC}/c4a/testshell/java/src/org/chromium/content/common/common.aidl
cp ${CHROME_SRC}/media/base/android/java/src -r ${CHROME_SRC}/c4a/testshell/java/
find ${CHROME_SRC}/c4a/testshell -name ".svn" |xargs rm -rf
cp ${CHROME_SRC}/net/android/java/src -r ${CHROME_SRC}/c4a/testshell/java/
find ${CHROME_SRC}/c4a/testshell -name ".svn" |xargs rm -rf
cp ${CHROME_SRC}/ui/android/java/src -r ${CHROME_SRC}/c4a/testshell/java/
find ${CHROME_SRC}/c4a/testshell -name ".svn" |xargs rm -rf

cp ${CHROME_SRC}/chrome/android/testshell/res -r ${CHROME_SRC}/c4a/testshell/java/
find ${CHROME_SRC}/c4a/testshell -name ".svn" |xargs rm -rf
cp ${CHROME_SRC}/out/Debug/chromium_testshell/libs -r ${CHROME_SRC}/c4a/testshell/java/
cp ${CHROME_SRC}/out/assets/chrome/* ${CHROME_SRC}/c4a/testshell/java/assets/

# remove .svn directory
find ${CHROME_SRC}/c4a/testshell -name ".svn" |xargs rm -rf
